#include "ofApp.h"

//#define DEBUG_MODE

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetLogLevel(OF_LOG_SILENT);

	fbo.allocate( ofGetWidth(), ofGetHeight() );
    
#ifdef TARGET_LINUX_ARM
    glLineWidth(5.0f);
#endif

	fragShader.allocate( fbo );
	fragShader.horizontal = true;
	fragShader.vertical = true;

	ofBackground(0);
	ofSetColor(255);
	ofSetVerticalSync(true);
	ofSetFrameRate(60);
	ofSetLogLevel(OF_LOG_VERBOSE);


	french_model = true;

	ofDirectory dir;
	dir.listDir("vec");
	model_files = dir.getFiles();

#ifndef DEBUG_MODE
	//    load_model(model_files[0]);
	word2vecEN.load_bin("vec/en_model.bin");
	word2vecFR.load_bin("vec/fr_model.bin");
#endif

	myFont.load("Electrolize-Regular.ttf", 32);
	smallerFont.load("Electrolize-Regular.ttf", 20);

	icoSphere = ofIcoSpherePrimitive(250, 4);
	icoSphere.setPosition( ofGetWidth() * .5/* + ofGetScreenWidth() *  1/4.f*/, ofGetHeight() * 0.5f, 0);
	icoSphere.setResolution(2);
	defaultMesh = icoSphere.getMesh();

	vbo = ofVbo();

	cout << "icosphere vertices: " << icoSphere.getMesh().getVertices().size() << " normals: " << icoSphere.getMesh().getNumNormals() << " indices: " << icoSphere.getMesh().getNumIndices() << endl;

	//#ifdef TARGET_LINUX_ARM
	// longer latency for Raspberry PI
	int ticksPerBuffer = 16; // 32 * 64 = buffer len of 2048
	int numInputs = 0; // no built in mic
	//#else
	//        int ticksPerBuffer = 8; // 8 * 64 = buffer len of 512
	//        int numInputs = 1;
	//#endif
	cout << ofFilePath::getCurrentWorkingDirectory() << endl;
	ofSoundStreamSettings settings;
	settings.numInputChannels = 0;
	settings.numOutputChannels = 2;
	settings.sampleRate = 44100;
	settings.bufferSize = ofxPd::blockSize() * ticksPerBuffer;
	settings.setInListener(this);
	settings.setOutListener(this);
	ofSoundStreamSetup(settings);

	// setup Pd
	//
	// set 4th arg to true for queued message passing using an internal ringbuffer,
	// this is useful if you need to control where and when the message callbacks
	// happen (ie. within a GUI thread)
	//
	// note: you won't see any message prints until update() is called since
	// the queued messages are processed there, this is normal
	//
	if(!pd.init(2, numInputs, 44100, ticksPerBuffer, true)) {
		OF_EXIT_APP(1);
	}

	cout << "listing pd directory" << endl;
	dir.listDir("pd");

	pd.addToSearchPath("pd");
	pd.openPatch("pd/AUTOMATONISM/main.pd");

	pd.subscribe("env_follow");
	pd.addReceiver(*this); // automatically receives from all subscribed sources

	pd.start();

	// default vectors

	ignore_case = true;

	// add emotional words to each vector of vectors, from most severe to least severe

	// this creates a vector space along a certain emotional path, that can be compared with the input vector

	vector < vector<float> > emotionVector;

	map< string, vector< vector<float> > > emotionMapSingleLanguage;

	for (int i = 0; i <= emotions.size(); i++) {
		if (i % 3 == 0 && i != 0) {
			emotionMapSingleLanguage.insert( { emotions.at(i - 3), emotionVector } );
			emotionVector.clear();
            if (i == 24 || i == emotions.size()) {
                emotionMap.insert( { i == 24 ? "EN" : "FR", emotionMapSingleLanguage } );
                emotionMapSingleLanguage.clear();
            }
		}

		if (i < emotions.size()) {
			emotionVector.push_back(word2vecEN.word_vector(emotions.at(i), ignore_case));
        }
	}
}

//--------------------------------------------------------------
void ofApp::load_model(ofFile file) {
	if(file.getExtension() == "csv") word2vecEN.load_csv(file.getAbsolutePath());
	else word2vecEN.load_bin(file.getAbsolutePath());
}

template <typename T>
double vectors_distance(const std::vector<T>& a, const std::vector<T>& b)
{
	std::vector<double>    auxiliary;

	std::transform (a.begin(), a.end(), b.begin(), std::back_inserter(auxiliary),//
					[](T element1, T element2) {return pow((element1-element2),2);});
	auxiliary.shrink_to_fit();

	return std::sqrt(std::accumulate(auxiliary.begin(), auxiliary.end(), 0.0));
} // end template vectors_distance

//--------------------------------------------------------------
void ofApp::process_string(const string& str_input) {
	// input can be a single word, or a phrase (sequence of words), or simple arithmetic operation. Split into tokens.

	//reset mesh
	for (int i = 0; i < defaultMesh.getNumVertices(); i++) {
		ofVec3f vert = defaultMesh.getVertices()[i];
		icoSphere.getMesh().setVertex(i, vert);
	}

	auto tokens = ofSplitString(str_input, " ", true, true);

	int op = 0; // arithmetic operator to perform

	// iterate all tokens (words and maths operators)
	for(auto&& t : tokens) {

		// if maths operator, save sign and write to display string
		if(t == "+") { op = 1; str_results << t << endl; }
		else if(t == "-") { op = -1; str_results << t << endl; }
		else {
			// otherwise get vector for word

			auto v = french_model ? word2vecFR.word_vector(t, true) : word2vecEN.word_vector(t, true);

			// if word found...
			if(!v.empty()) {
				// if we have a maths operator on stack, apply it, otherwise save vector as is
				if(op) vec = msa::vector_utils::weighted_sum(vec, 1.0f, v, 1.0f * op);
				else vec = v;

				// find distance from other vectors

				cout << "\nentered word " << str_input << endl;

				float minDist = 999999.0;
				float maxDist = 0.0;

				for (int i = 0; i < 8; i++) { //8 base emotions for both languages
					// take average of distances between all three words in the emotion vector
					cout << "\ni: " << i << endl;

                    vector < vector<float> > emotionVector = emotionMap[french_model ? "FR" : "EN" ][emotions[i * 3]];
					for (int j = 0; j < 3; j++) {

						float avgDist = vectors_distance(vec, french_model ? word2vecFR.word_vector(emotions[(i * 3 + j) + 24]) : word2vecEN.word_vector(emotions[i * 3 + j], true));

						cout << "\navgDist: " << avgDist << " emotion: " << emotions[(i * 3 + j) + (french_model ? 24 : 0)] << endl;

						switch (i) {
							case 0:
								rageDist = avgDist;
								break;
							case 1:
								vigilDist = avgDist;
								break;
							case 2:
								joyDist = avgDist;
								break;
							case 3:
								trustDist = avgDist;
								break;
							case 4:
								fearDist = avgDist;
								break;
							case 5:
								surpriseDist = avgDist;
								break;
							case 6:
								griefDist = avgDist;
								break;
							case 7:
								disgustDist = avgDist;
								break;
							default:
								break;
						}

						if (avgDist < minDist) {
							minDist = avgDist;
							closestEmotionIndex = i * 3 + j;
							baseEmotionIndex = i;
							closestHue = emotionHues[baseEmotionIndex];

							cout << "\ni: " << i << " emotionIndex " << baseEmotionIndex << "setting closest emotion: " << emotions[(i * 3 + j) + (french_model ? 24 : 0)] << " closest hue: " << emotionHues[baseEmotionIndex] << " sending " << baseEmotionIndex << " to patch" << endl;
						} else if (avgDist > maxDist) {
							maxDist = avgDist;
						}

					}
//					cout << "normalized distances: " << rageDist << endl << vigilDist << endl << joyDist << endl << trustDist << endl << fearDist << endl << surpriseDist << endl << griefDist << endl < disgustDist << endl;

//					avgDist /= 3.0;
				}

				// normalize all emotion distances
				rageDist = (maxDist - rageDist) / maxDist;
				vigilDist = (maxDist - vigilDist) / maxDist;
				joyDist = (maxDist - joyDist) / maxDist;
				trustDist = (maxDist - trustDist) / maxDist;
				fearDist = (maxDist - fearDist) / maxDist;
				surpriseDist = (maxDist - surpriseDist) / maxDist;
				griefDist = (maxDist - griefDist) / maxDist;
				disgustDist = (maxDist - disgustDist) / maxDist;

				pd.sendFloat("baseEmotion", (float)baseEmotionIndex);
				pd.sendFloat("emotion", (float)closestEmotionIndex);
				pd.sendFloat("rageDist", rageDist);
				pd.sendFloat("vigilDist", vigilDist);
				pd.sendFloat("joyDist", joyDist);
				pd.sendFloat("trustDist", trustDist);
				pd.sendFloat("fearDist", fearDist);
				pd.sendFloat("surpriseDist", surpriseDist);
				pd.sendFloat("griefDist", griefDist);
				pd.sendFloat("disgustDist", disgustDist);

				// write to display string
				str_results << t << ": " << msa::vector_utils::to_string(vec) << endl;
			} else {
				str_results << t << ": " << "not found" << endl;
				cout << "\n" << t << "not found " << endl;
			}
			op = 0; // clear maths operator stack
		}
	}

	float average = 0.0f;
	for (int i = 0; i < vec.size(); i++) {
		average += vec.at(i);
	}
	average /= vec.size();

	printf("sending average: %f", average);
	pd.sendFloat("pitch", average);
	pd.sendFloat("lfo_freq", average);
}

float nfmod(float a,float b)
{
	return a - b * floor(a / b);
}

//--------------------------------------------------------------
void ofApp::update() {
	//    sandbox.update();
	if(pd.isQueued()) {
		// process any received messages, if you're using the queue and *do not*
		// call these, you won't receieve any messages or midi!
		pd.receiveMessages();
		pd.receiveMidi();
	}


	fbo.begin();
	ofClear(0, 0, 0, 0);

	float spinX = sin(ofGetElapsedTimef()*.35f);
	float move = sin(ofGetElapsedTimef()*5);
	icoSphere.rotateDeg(spinX, (ofGetFrameNum()/30) % 2 == 0 ? 1.0 : 0.0, 1.0, 0.0);
	icoSphere.getMesh().clearColors();

	ofFloatColor newColor = ofFloatColor(0.0, 0.0, 0.0);

	vector<ofFloatColor> sphereColors;
	vector <ofVec3f> sphereVerts;

	newPosition = ofVec3f( ofGetScreenWidth() * 0.5, ofGetScreenHeight() * 0.5, 0.5);

	for (int i = 0; i < icoSphere.getMesh().getNumVertices(); i++) {
		ofVec3f vert = icoSphere.getMesh().getVertices()[i];


		float timeScale = 3.0 + baseEmotionIndex;
		float time = ofGetElapsedTimef() * timeScale;
		float displacementScale = 2.5 + baseEmotionIndex;

		float dimen = vec.size() ? vec[i % 300] : 0.1;
		float dimen2 = vec.size() ? vec[i + 1 % 300] : 0.1;
		float dimen3 = vec.size() ? vec[i + 2 % 300] : 0.1;

		vert.x += sin(time * dimen) * dimen * displacementScale * (rageDist + fearDist);
		vert.y += sin(time * dimen2) * dimen2 * displacementScale * (joyDist + griefDist);
		vert.z += sin(time * dimen3) * dimen3 * displacementScale * ((vigilDist + trustDist) * (surpriseDist + disgustDist));

		float hueMod = ((sin(time * dimen) * rageHue) * rageDist
		+ (sin(dimen2 * time) * vigilHue) * vigilDist
		+ (sin(dimen3 * time) * joyHue) * joyDist
		+ (sin(dimen * time) * trustHue) * trustDist
		+ (sin(dimen2 * time) * fearHue) * fearDist
		+ (sin(dimen3 * time) * surpriseHue) * surpriseDist
		+ (sin(dimen * time) * griefHue) * griefDist
		+ (sin(dimen2 * time) * disgustHue) * disgustDist) * 0.125;

		newColor = ofFloatColor::fromHsb(nfmod(dimen * sin(time) * 0.5 + closestHue + (hueMod), 1.0), 1.0, 0.75 + dimen3 * 0.5/* 1.0*/);

		sphereColors.push_back(newColor);
		icoSphere.getMesh().addColor(newColor);
		icoSphere.getMesh().setVertex(i, vert);
		vert = vert + newPosition;

		sphereVerts.push_back(vert);
	}

    vbo.setColorData(&sphereColors[0], icoSphere.getMesh().getNumColors(), GL_DYNAMIC_DRAW);
	vbo.setVertexData(&sphereVerts[0], icoSphere.getMesh().getNumVertices(), GL_DYNAMIC_DRAW);
	vbo.setIndexData(icoSphere.getMesh().getIndexPointer(), icoSphere.getMesh().getNumIndices(), GL_DYNAMIC_DRAW);
	vbo.drawElements(GL_LINES, icoSphere.getMesh().getNumVertices());

	fbo.end();
	fragShader.apply( fbo );
}

void ofApp::exit() {
	ofSoundStreamStop();
}

void ofApp::draw() {

	stringstream s;
	s << ">" << str_input << ((ofGetFrameNum()/30) % 2 == 0 ? "_" : "") << endl;
	myFont.drawString(s.str(), 20, ofGetHeight() * 0.35);

	smallerFont.drawString(french_model ? "[1]: français *\n[2]: english" : "[1]: français \n[2]: english *", 10, 30);
	fbo.draw( 0, 0 );
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	str_results.str("");

	bool do_search = auto_search;

	switch(key) {
		case OF_KEY_TAB:
			str_input = "";
			break;

		case OF_KEY_BACKSPACE:
//			str_input = str_input.substr(0, str_input.size() - 1);
			ofUTF8Erase(str_input, ofUTF8Length(str_input) - 1, 1);
			break;

		case OF_KEY_RETURN:
			do_search = true;
			break;

		case '1':
			french_model = true;
			break;

		case '2':
			french_model = false;
			break;
		case ' ':
		case '+':
		case '-':
			str_input += key;
			break;

		default:
			ofUTF8Append(str_input, key);
//			str_input += key;
			cout << str_input << endl;
			break;

	}

	if(do_search) {
		cout << "\nprocessing string: " << str_input << endl;
		process_string(str_input);
	}

	//    pd.sendBang("nudge");
}

void ofApp::keyReleased(int key) {}

void ofApp::mouseMoved(int x, int y ) {}

void ofApp::mouseDragged(int x, int y, int button) {}

void ofApp::mousePressed(int x, int y, int button) {

}

void ofApp::mouseReleased(int x, int y, int button) {

}

void ofApp::mouseEntered(int x, int y) {

}

void ofApp::mouseExited(int x, int y) {

}

void ofApp::windowResized(int w, int h) {
	fbo.allocate( ofGetWidth(), ofGetHeight() );
}

void ofApp::dragEvent(ofDragInfo dragInfo) {

}

void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::audioReceived(float * input, int bufferSize, int nChannels) {
	pd.audioIn(input, bufferSize, nChannels);
}

//--------------------------------------------------------------
void ofApp::audioRequested(float * output, int bufferSize, int nChannels) {
	pd.audioOut(output, bufferSize, nChannels);
}

//--------------------------------------------------------------
void ofApp::print(const std::string& message) {
	cout << message << endl;
}

void ofApp::receiveFloat(const std::string& dest, float value) {

}

void ofApp::receiveBang(const std::string& dest) {

}
