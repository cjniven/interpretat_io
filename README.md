# Interpretat.io

by Isabelle Bédard and Chris Niven

![Screenshot of Interpretat.io](interpretat_io_screenshot.png)

## Introduction

Interpretat.io is a digital synesthetic dictionary. You can look up words in English or 
French and experience a unique audiovisual vibration.

Interpretat.io is based off a [demo project](https://github.com/memo/ofxMSAWord2Vec) for ofxMSAWord2Vec, created by Memo Atken.

Word2Vec is a technique for creating contextual mappings of words from a text input source.
This application uses pre-trained models based off of the English and French versions of Wikipedia. Each word entered will output a unique multi-dimensional vector of floating point numbers, with related words being closer together within the vector space.

The idea behind the project was to create a map of Word2Vec output for base emotions, based on Robert Plutchik's "[wheel of emotions](https://en.wikipedia.org/wiki/Contrasting_and_categorization_of_emotions#Plutchik.27s_wheel_of_emotions)" Every entered word would be compared against the eight base emotions at different intensities (e.g. for the base emotion "rage", the input word would be compared against "rage, "anger", and "annoyance"). The closest emotion that can be represented by the input word will be selected and be reflected in the character of the visualisation and sound.


Interpretat.io is a prototype! The code is rough and in need of refactoring!

## Installing

This program has been tested on Raspberry Pi 3 B+ and Mac OS X 10.13.

### Raspberry Pi (tested on model 3 B+ with Rasbpian Stretch)

### This guide has been modified from the official [documentation](https://openframeworks.cc/setup/raspberrypi/raspberry-pi-getting-started/) for installing OpenFrameworks on the Raspberry Pi.  

1. Ensure that the system is ready to install OpenFrameworks
a) install packages
```
sudo apt-get clean
sudo apt-get update
sudo apt-get upgrade
```

b) Ensure the Pi has enough RAM to compile OpenFrameworks
```
sudo raspi-config
```
	Select `1 Expand Filesystem` and hit Enter
	Select `8 Advanced Options` and hit Enter
	       > Select `A3 Memory Split` and hit Enter
	       > Type `64` and hit `<ok>`


2. Download and uncompress [OpenFrameworks 10, release candidate 4 for armv6](http://openframeworks.cc/versions/v0.10.0RC4/of_v0.10.0RC4_linuxarmv6l_release.tar.gz)
```
cd
wget http://openframeworks.cc/versions/v0.10.0RC4/of_v0.10.0RC4_linuxarmv6l_release.tar.gz --no-check-certificate
mkdir openFrameworks
tar vxfz of_v0.10.0RC4_linuxarmv6l_release.tar.gz -C openFrameworks --strip-components 1
```

3. Run the prerequesite scripts for building OpenFrameworks
``` 
cd /home/pi/openFrameworks/scripts/linux/debian
sudo ./install_dependencies.sh
sudo ./install_codecs.sh
```

4. Install addons
```
cd /home/pi/openFrameworks/addons
git clone https://github.com/memo/ofxMSAWord2Vec.git
git clone https://github.com/memo/ofxMSAVectorUtils.git
git clone https://github.com/danomatika/ofxPd.git
git clone https://github.com/danomatika/ofxMidi.git
git clone https://github.com/npisanti/ofxDotFrag.git
```
6. Set C++ standard library to C++14
This program requires some c++14 features to compile correctly. To enable this, you need to edit the 
```
cd /home/pi/openFrameworks/libs/openFrameworksCompiled/project/makefileCommon/
nano config.linux.common.mk
```
Uncomment line 456 and replace it with the following:
```
PLATFORM_CXX += -std=c++14
```
Save the file.

7. Build OpenFrameworks
```
make Release -C /home/pi/openFrameworks/libs/openFrameworksCompiled/project
```

8. Install and run the project
```
cd /home/pi/openFrameworks/apps/myApps
git clone https://gitlab.com/cjniven/interpretat_io.git
cd interpretat_io
make && make RunRelease
```

If the graphics are laggy, repeat step 1.b) and allocate a larger RAM size to the GPU (e.g. 128 or 256 MB).

9. Enjoy!

## Known Issues

Occasionally entering words will not work after the program is launched. If this happens, try switching languages (by pressing 1 or 2) or continue entering different words until the input is accepted.

## Acknowledgements

Special thanks to Techno Culture Club co-directors  Malaurie Barlett and Ariel Harlap, as well as the rest of the team!

Thank you for trying our prototype. Please don't hesitate to send any questions to cjniven@gmail.com!