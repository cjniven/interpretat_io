#pragma once

#include "ofMain.h"
#include "ofxMSAWord2Vec.h"
#include "ofxMSAVectorUtils.h"
#include "ofxPd.h"
#include "ofxDotFrag.h"

using namespace pd;

class ofApp : public ofBaseApp, public PdReceiver {

public:
    void setup();
    void update();
    void draw();
    void exit();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    void load_model(ofFile file);
    void process_string(const string& str_input);
    
    // audio callbacks
    void audioReceived(float * input, int bufferSize, int nChannels);
    void audioRequested(float * output, int bufferSize, int nChannels);
    
    // pd message receiver callbacks
    void print(const std::string& message);
    void receiveFloat(const std::string& dest, float value);
	void receiveBang(const std::string& dest);
    
    ofxPd pd;
    msa::Word2Vec word2vecEN;
    msa::Word2Vec word2vecFR;
    bool french_model = false;
    
    string str_input = "";      // string entered by user
    stringstream str_results;   // string containing results to display on screen
    
    // paths to various different model files
    vector<ofFile> model_files;
    
    int model_index = 0;        // which model to use (from array above)
    int dist_mode = 0;          // how to measure distance  between vectors. 0: cosine similarity (norm dot product), 1: L2 norm (euclidean distance)
    bool auto_search = false;    // automatically search with every key press (could be slow on slow computers)
    bool ignore_case = false;   // do case insensitive search (can be slower)
    
    ofIcoSpherePrimitive icoSphere;
    ofMesh defaultMesh;
    
    ofTrueTypeFont myFont;
	ofTrueTypeFont smallerFont;
    
    float warp = 0;

	map< string, map < string, vector < vector<float> > > > emotionMap;

	vector<string> emotions = {"rage", "anger", "annoyance", "vigilance", "anticipation", "interest", "ecstasy", "joy", "serenity", "admiration", "trust", "acceptance", "terror", "fear", "apprehension", "amazement", "surprise", "distraction", "grief", "sadness", "pensiveness", "loathing", "disgust", "boredom" ,

        /* FRANCAIS */
        
        "rage", "colère", "agacement", "vigilance", "anticipation", "intérêt", "extase", "joie", "sérénité", "admiration", "confiance", "acceptation", "terreur", "peur", "appréhension", "stupéfaction", "surprise", "distraction", "deuil", "tristesse", "réflexion", "répugnance", "dégoût", "ennui" };
	
	vector<float> emotionHues = { 0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875 };
	int baseEmotionIndex = 0;
	int closestEmotionIndex = 0;

	float rageHue = 0.0;
	float vigilHue = 0.125;
	float joyHue = 0.20;
	float trustHue = 0.375;
	float fearHue = 0.55;
	float surpriseHue = 0.625;
	float griefHue = 0.75;
	float disgustHue = 0.875;
	float closestHue = 0.0;
    float rageDist;
    float vigilDist;
    float joyDist;
    float trustDist;
    float fearDist;
    float surpriseDist;
	float griefDist;
	float disgustDist;
    
    ofVec3f newPosition;
	ofFbo fbo;
	ofVbo vbo;
    ofx::dotfrag::MirrorAxis fragShader;
    
#ifndef DEBUG_MODE
    vector<float> vec;
#else
    vector<float> vec = {0.0228814, 0.499724, 0.0432677, 0.117513 , -0.016054 , -0.226031, 0.0798707 , -0.0729062 , -0.205718 , -0.00236381, 0.395409, 0.150045, 0.225318, 0.210806, 0.100972 , -0.0208725 , -0.0207162 , -0.104779, 0.0531831 , -0.0182152, 0.0899741 , -0.278092 , -0.0746878 , -0.0257769 , -0.338883, 0.224434, 0.109231, 0.0188447, 0.174336, 0.2553, 0.135843, 0.00649894 , -0.260276, 0.23639, 0.0423837 , -0.00300307 , -0.133471 , -0.31419, 0.439702, 0.136657, 0.0677481, 0.214794 , -0.0524055 , -0.111528, 0.155818, 0.0990237, 0.188873, 0.105294, 0.268443 , -0.120335 , -0.16159 , -0.291438 , -0.0657318, 0.152965 , -0.0658432, 0.275667, 0.0681076 , -0.0776423 , -0.262174, 0.0731357, 0.17132 , -0.18784, 0.0144867 , -0.17876, 0.454173, 0.049198 , -0.385188, 0.0661215 , -0.0119194, 0.296041, 0.198699, 0.111223 , -0.0568522, 0.250211 , -0.255532 , -0.211977, 0.129808, 0.296255, 0.0438812 , -0.0516762 , -0.207352 , -0.00836841 , -0.0374629 , -0.0816997 , -0.00878212, 0.0776977 , -0.246958 , -0.0241427, 0.165904 , -0.0303338, 0.196287, 0.0384254, 0.158193, 0.165421 , -0.158585, 0.204352 , -0.0979015 , -0.0162649, 0.334785 , -0.109971, 0.0839673 , -0.228648 , -0.0828027 , -0.673428 , -0.26273, 0.129275 , -0.0465517, 0.0530674 , -0.0926536 , -0.00596435, 0.0695252, 0.00829177, 0.261626, 0.353033, 0.468352, 0.0317528, 0.0368782 , -0.124949 , -0.0972988, 0.223996 , -0.00696693, 0.00902626 , -0.298845, 0.342083 , -0.0664025 , -0.31283 , -0.336969 , -0.19577, 0.286895, 0.216666, 0.0370417 , -0.0691576 , -0.0249003 , -0.367648 , -0.00952739 , -0.001438, 0.403704, 0.277948 , -0.31529, 0.327562, 0.0748817 , -0.322055, 0.170096 , -0.0958263, 0.000238502 , -0.218211 , -0.323273 , -0.100134, 0.0922847 , -0.179725, 0.243447, 0.403378, 0.0128944, 0.326533, 0.0537288, 0.290564, 0.162896, 0.0634416 , -0.12736, 0.259803 , -0.0607582 , -0.186665 , -0.078835 , -0.0590909 , -0.01831 , -0.170953 , -0.104091, 0.0195216 , -0.0997847, 0.0565044 , -0.114188 , -0.011007 , -0.200285, 0.0528825 , -0.0246093, 0.041091 , -0.0884277, 0.347131 , -0.136701, 0.0293608, 0.0653872, 0.12202 , -0.186528, 0.371422, 0.388689 , -0.241222, 0.129608, 0.0116562 , -0.401305, 0.300967, 0.0254038 , -0.166486 , -0.119381 , -0.0968833, 0.21478 , -0.104192, 0.0713445, 0.0841732, 0.199916, 0.164799 , -0.0401662, 0.219364, 0.0330066 , -0.198688 , -0.24309, 0.0289507 , -0.141209, 0.106723 , -0.425159, 0.134891 , -0.0288392, 0.369581, 0.0993513 , -0.0680319, 0.180733, 0.227479 , -0.126302, 0.377237 , -0.182791 , -0.180077, 0.304201, 0.103792, 0.0343902, 0.011016, 0.361927, 0.0335527 , -0.0955957 , -0.0738037 , -0.346478 , -0.2162 , -0.0225905 , -0.0343607 , -0.30835 , -0.264319, 0.06522, 0.730869 , -0.209505 , -0.205309, 0.0494744, 0.0501255, 0.117737 , -0.189307 , -0.10058 , -0.129465 , -0.236573, 0.0965829 , -0.167456, 0.0777575, 0.0285761, 0.0484141 , -0.269753, 0.0330728, 0.254625 , -0.0542828, 0.0141713 , -0.0425359};
#endif
};
