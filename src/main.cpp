//                                     __
//    ____ ___  ___  ____ ___  ____   / /__   __
//   / __ `__ \/ _ \/ __ `__ \/ __ \ / __/ | / /
//  / / / / / /  __/ / / / / / /_/ // /_ | |/ /
// /_/ /_/ /_/\___/_/ /_/ /_/\____(_)__/ |___/
//
//
// Created by Memo Akten, www.memo.tv
//
// Modified by Chris Niven for Techno Culture Club co-residency, July-October 2018

#include "ofApp.h"
#include "ofMain.h"

//========================================================================
int main( ){
  ofGLESWindowSettings settings;
  settings.glesVersion = 2;
  settings.setSize(480, 360);
  settings.windowMode = OF_FULLSCREEN;
#ifdef TARGET_LINUX_ARM
  ofCreateWindow(settings);
#else
	ofSetupOpenGL(1024,768, OF_FULLSCREEN);            // <-------- setup the GL context
//	ofSetupOpenGL(1024,768, OF_WINDOW); 
#endif
  ofRunApp(new ofApp());
}
